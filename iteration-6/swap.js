const words = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño'];

function swap(array, index1, index2) {
    const word1 = array[index1];
    const word2 = array[index2];
    array.splice(index1, 1, word2);
    array.splice(index2, 1, word1);
    return array;
}

let wordsChanged = swap(words, 1 , 3);
wordsChanged = swap(words, 0 , 2);
console.log(wordsChanged);