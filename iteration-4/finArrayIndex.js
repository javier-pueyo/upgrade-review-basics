/* Crea una función llamada findArrayIndex que reciba como parametros un array de textos y un texto y devuelve la posición del array cuando el valor del array sea igual al valor del texto que enviaste como parametro. Haz varios ejemplos y compruebalos. */
const arrayWords = ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote']

function findArrayIndex(array, text) {
    let wordIndex;
    array.forEach((element, index) => {
        if (element == text) {
           wordIndex = index;
        }
    });
    return wordIndex;
}

console.log(findArrayIndex(arrayWords, 'Mosquito'));
/* console.log(findArrayIndex(arrayWords, 'Salamandra'));
console.log(findArrayIndex(arrayWords, 'Polilla')); */


