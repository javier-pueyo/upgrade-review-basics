function rollDice(numeroCaras) {
    const min = 1;
    const max = numeroCaras;
    
    const randomNumberMinMax = Math.round(Math.random()*(max - min)+min);
    return randomNumberMinMax;
}

const numeroCarasDado = 30;
console.log(`Tirada del dado de ${numeroCarasDado} caras. El resultado fue: ${rollDice(numeroCarasDado)}`);
